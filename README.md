# acousticmeasures
python code for acoustic measurements:

acousticweighting.py consists of 4 functions to apply a or c weighting to measurement data.

freq2freqtransforms.py consists of a function to get a fractional octave transformation matrix to 
transform fft based data to fractional bands. 
